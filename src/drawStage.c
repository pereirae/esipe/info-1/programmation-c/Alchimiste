#include "../include/drawStage.h"

void drawStage(SDLRessources *sdlRessources, GameData *gameData) {
    int windowWidth;
    int windowHeight;
    SDL_Color *color = NULL;
    if ((color = malloc(sizeof(SDL_Color))) == NULL) {
        return;
    }
    SDL_GetWindowSize(sdlRessources->window, &windowWidth, &windowHeight);
    getCurrentRenderColor(sdlRessources->renderer, color);
    drawBoardFrame(sdlRessources);
    drawAvailableBalls(sdlRessources, gameData->availableColors, windowHeight);
    drawBoardContent(sdlRessources, gameData->board, windowHeight);
    drawMolecule(sdlRessources, gameData->molecule);
    setCurrentRenderColor(sdlRessources->renderer, color);
    free(color);
}

void drawBoardFrame(SDLRessources *sdlRessources) {
    int windowWidth;
    int windowHeight;
    SDL_Rect rect = {0, 0, 1, 1};
    SDL_Color *color = NULL;
    if ((color = malloc(sizeof(SDL_Color))) == NULL) {
        return;
    }
    SDL_GetWindowSize(sdlRessources->window, &windowWidth, &windowHeight);
    rect.h += BALL_SIZE * BOARD_ROWS;
    rect.w += BALL_SIZE * BOARD_COLS;
    rect.y = windowHeight - rect.h;
    rect.x = BOARD_OFFSET;
    getSDLColor(WHITE, color);
    setCurrentRenderColor(sdlRessources->renderer, color);
    SDL_RenderDrawRect(sdlRessources->renderer, &rect);
    free(color);
}

void drawAvailableBalls(SDLRessources *sdlRessources, AvailableColors *availableColors, int wHeight) {
    int i;
    int x = BOARD_OFFSET / 2;
    SDL_Color *color = NULL;
    if ((color = malloc(sizeof(SDL_Color))) == NULL) {
        return;
    }
    for (i = 0; i < availableColors->size; i++) {
        if (availableColors->colors[i]) {
            int y = wHeight - BALL_SIZE * (i + 1);
            getSDLColor(availableColors->colors[i], color);
            SDL_SetRenderDrawColor(sdlRessources->renderer, color->r, color->g, color->b, color->a);
            /* Ajout d'une marge entre les boules */
            y -= i * 5;
            drawCircle(sdlRessources->renderer, x, y, BALL_SIZE / 2);
        }
    }
    free(color);
}

void drawBoardContent(SDLRessources *sdlRessources, Board *board, int wHeight) {
    int i, j;
    SDL_Color *color = NULL;
    if ((color = malloc(sizeof(SDL_Color))) == NULL) {
        return;
    }
    for (i = 0; i < board->nbRows; i++) {
        for (j = 0; j < board->nbCols; j++) {
            if (board->content[i][j] != 0) {
                int x = j * BALL_SIZE + BALL_SIZE / 2 + BOARD_OFFSET;
                int y = wHeight - BALL_SIZE * board->nbRows;
                y += i * BALL_SIZE + BALL_SIZE / 2;
                getSDLColor(board->content[i][j], color);
                setCurrentRenderColor(sdlRessources->renderer, color);
                drawCircle(sdlRessources->renderer, x, y, BALL_SIZE / 2);
            }
        }
    }
    free(color);
}

void drawMolecule(SDLRessources *sdlRessources, Molecule *molecule) {
    int xCircle1, xCircle2, yCircle1, yCircle2;
    SDL_Color *color = NULL;
    if ((color = malloc(sizeof(SDL_Color))) == NULL) {
        return;
    }
    xCircle1 = BOARD_OFFSET + BALL_SIZE / 2;
    xCircle2 = BOARD_OFFSET + BALL_SIZE / 2;
    yCircle1 = 100;
    yCircle2 = 100;
    xCircle1 += molecule->pos * BALL_SIZE;
    if (molecule->alignment) {
        xCircle2 += molecule->pos * BALL_SIZE;
        yCircle2 -= BALL_SIZE;
    } 
    else {
        xCircle2 = xCircle1 + BALL_SIZE;
    }
    getSDLColor(molecule->color, color);
    setCurrentRenderColor(sdlRessources->renderer, color);
    drawCircle(sdlRessources->renderer, xCircle1, yCircle1, BALL_SIZE / 2);
    drawCircle(sdlRessources->renderer, xCircle2, yCircle2, BALL_SIZE / 2);
    free(color);
}

void getCurrentRenderColor(SDL_Renderer *renderer, SDL_Color *color) {
    SDL_GetRenderDrawColor(renderer, &color->r, &color->g, &color->b, &color->a);
}

void setCurrentRenderColor(SDL_Renderer *renderer, SDL_Color *color) {
    SDL_SetRenderDrawColor(renderer, color->r, color->g, color->b, color->a);
}
