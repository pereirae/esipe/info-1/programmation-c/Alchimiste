#include "../include/alchimiste.h"
#include "../include/molecule.h"

Molecule *createMolecule(int color) {
    Molecule *molecule = NULL;
    if ((molecule = malloc(sizeof(Molecule))) == NULL) {
        return NULL;
    }
    molecule->color = color;
    molecule->pos = getRandomValueBetween(0, BOARD_COLS - 1);
    if (molecule->pos == BOARD_COLS - 1) {
        molecule->alignment = 1;
    }
    else {
        molecule->alignment = getRandomValueBetween(0, 1);
    }
    return molecule;
}

void moveMoleculeRight(Molecule *molecule) {
    if (molecule == NULL) {
        return;
    }
    if (molecule->alignment == 1) {
        if (molecule->pos < BOARD_COLS - 1) {
            molecule->pos++;
        }
    }
    else if (molecule->pos < BOARD_COLS - 2) {
        molecule->pos++;
    }
}

void moveMoleculeLeft(Molecule *molecule) {
    if (molecule == NULL) {
        return;
    }
    if (molecule->pos > 0) {
        molecule->pos--;
    }
}

void rotateMolecule(Molecule *molecule) {
    if (molecule->pos == BOARD_COLS - 1) {
        molecule->pos--;
    }
    if (molecule->alignment == VERTICAL) {
        molecule->alignment = HORIZONTAL;
    }
    else {
        molecule->alignment = VERTICAL;
    }
}

int getRandomValueBetween(int min, int max) {
    int i;
    int randomNbr = 0;
    for (i = 0; i < 20; i++) {
        randomNbr = rand();
    }
    return min + randomNbr % (max + 1 - min);
}

void destroyMolecule(Molecule *molecule) {
    free(molecule);
}

AvailableColors *initAvailableColors(int nbColors) {
    int i;
    AvailableColors *colors = NULL;
    if ((colors = malloc(sizeof(AvailableColors))) == NULL) {
        return NULL;
    }
    colors->size = 1;
    colors->nbColors = nbColors;
    if ((colors->colors = malloc(sizeof(int) * nbColors)) == NULL) {
        return NULL;
    }
    for (i = 0; i < nbColors; i++) {
        colors->colors[i] = 0;
    }
    colors->colors[0] = GREEN;
    return colors;
}

void enableColor(AvailableColors *availableColors, int color) {
    /* La dernière couleur disponible à déjà été atteinte */
    if (color > availableColors->nbColors) {
        return;
    }
    if (!availableColors->colors[color - 1]) {
        availableColors->colors[color - 1] = color;
        availableColors->size++;
    }
}

void destroyAvailableColors(AvailableColors *availableColors) {
    free(availableColors->colors);
    free(availableColors);
}
