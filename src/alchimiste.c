#include "../include/alchimiste.h"
#include "../include/drawStage.h"

void gameLoop(SDLRessources *sdlRessources) {
    SDL_bool quit = SDL_FALSE;
    GameData *gameData = NULL;
    if ((gameData = initGameData()) == NULL) {
        return;
    }
    while (!quit) {
        while (SDL_PollEvent(&sdlRessources->event)) {
            switch(sdlRessources->event.type) {
                case SDL_QUIT:
                    quit = SDL_TRUE;
                    break;
                case SDL_KEYUP:
                    handleKeyEvent(gameData, sdlRessources->event.key);
                    quit = isGameOver(gameData);
                    break;
            }
        }
        SDL_RenderClear(sdlRessources->renderer);
        drawStage(sdlRessources, gameData);
        SDL_RenderPresent(sdlRessources->renderer);
    }
    destroyGameData(gameData);
}

void handleKeyEvent(GameData *gameData, SDL_KeyboardEvent event) {
    int nextMoleculeColor;
    if (event.keysym.scancode == SDL_SCANCODE_RIGHT) {
        moveMoleculeRight(gameData->molecule);
    }
    if (event.keysym.scancode == SDL_SCANCODE_LEFT) {
        moveMoleculeLeft(gameData->molecule);
    }
    if (event.keysym.scancode == SDL_SCANCODE_UP) {
        rotateMolecule(gameData->molecule);
    }
    if (event.keysym.scancode == SDL_SCANCODE_DOWN) {
        if (putMoleculeInBoard(gameData->molecule, gameData->board)) {
            destroyMolecule(gameData->molecule);
            nextMoleculeColor = getRandomValueBetween(1, gameData->availableColors->size);
            gameData->molecule = createMolecule(nextMoleculeColor);
        }
        fillBoardGaps(gameData->board);
        processNeighbors(gameData);
    }
}

int isGameOver(GameData *gameData) {
    int i;
    /* Il est seulement nécessaire de vérifier les deux lignes les plus hautes
    du plateau de jeu */
    for(i = 0; i < gameData->board->nbCols - 1; i++) {
        if (gameData->board->content[0][i] == 0) {
            /* Un espace suffisant pour une molecule horizontal existe */
            if (gameData->board->content[0][i + 1] == 0) {
                return SDL_FALSE;
            }
            /* Un espace suffisant pour une molecule vertical existe */
            if (gameData->board->content[1][i] == 0) {
                return SDL_FALSE;
            }
        }
    }
    printf("Vous avez perdu !\n");
    return SDL_TRUE;
}

SDLRessources *createSDLRessources() {
    SDLRessources *sdlRessources = NULL;
    if ((sdlRessources = malloc(sizeof(SDLRessources))) == NULL) {
        fprintf(stderr, "Erreur lors de l'allocation mémoire des ressources SDL");
        return NULL;
    }
    sdlRessources->window = SDL_CreateWindow(W_TITLE, 
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 
        W_WIDTH, W_HEIGHT, 
        SDL_WINDOW_SHOWN);
    if (sdlRessources->window == NULL) {
        fprintf(stderr, "Erreur lors de la création de la fenêtre : %s\n", SDL_GetError());
        return NULL;
    }
    sdlRessources->renderer = SDL_CreateRenderer(sdlRessources->window, -1, SDL_RENDERER_PRESENTVSYNC |
                                                                              SDL_RENDERER_ACCELERATED);
    if (sdlRessources->renderer == NULL) {
        fprintf(stderr, "Erreur lors de la création du renderer : %s", SDL_GetError());
        return NULL;
    }
    return sdlRessources;
}

void cleanSDLRessources(SDLRessources *sdlRessources) {
    if (sdlRessources->renderer != NULL) {
        SDL_DestroyRenderer(sdlRessources->renderer);
    }
    if (sdlRessources->window != NULL) {
        SDL_DestroyWindow(sdlRessources->window);
    }
    free(sdlRessources);
}

void getSDLColor(int colorCode, SDL_Color *color) {
    color->a = SDL_ALPHA_OPAQUE;
    switch (colorCode) {
        case GREEN:
            color->r = 0;
            color->g = 255;
            color->b = 0;
            break;
        case ORANGE:
            color->r = 255;
            color->g = 125;
            color->b = 0;
            break;
        case RED:
            color->r = 255;
            color->g = 0;
            color->b = 0;
            break;
        case PURPLE:
            color->r = 100;
            color->g = 0;
            color->b = 130;
            break;
        case BLUE:
            color->r = 0;
            color->g = 0;
            color->b = 255;
            break;
        case LIGHT_GREEN:
            color->r = 115;
            color->g = 255;
            color->b = 115;
            break;
        case DARK_GREEN:
            color->r = 20;
            color->g = 90;
            color->b = 20;
            break;
        case YELLOW:
            color->r = 255;
            color->g = 255;
            color->b = 0;
            break;
        case GRAY:
            color->r = 117;
            color->g = 117;
            color->b = 117;
            break;
        case WHITE:
            color->r = 255;
            color->g = 255;
            color->b = 255;
            break;
        default:
            color->r = 0;
            color->g = 0;
            color->b = 0;
            break;  
    }
}
