#include "../include/alchimiste.h"


GameData *initGameData() {
    GameData *gameData = NULL;
    if ((gameData = malloc(sizeof(GameData))) == NULL) {
        return NULL;
    }
    if ((gameData->board = createBoard(BOARD_ROWS, BOARD_COLS)) == NULL) {
        free(gameData);
        return NULL;
    }
    if ((gameData->availableColors = initAvailableColors(10)) == NULL) {
        destroyBoard(gameData->board);
        free(gameData);
        return NULL;
    }
    if ((gameData->molecule = createMolecule(GREEN)) == NULL) {
        destroyBoard(gameData->board);
        destroyAvailableColors(gameData->availableColors);
        free(gameData);
        return NULL;
    }
    srand (time(NULL));
    return gameData;
}

void destroyGameData(GameData *gameData) {
    destroyBoard(gameData->board);
    destroyAvailableColors(gameData->availableColors);
    free(gameData->molecule);
    free(gameData);
}
