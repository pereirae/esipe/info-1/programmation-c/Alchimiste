#include "../include/circle.h"

void drawOuterCircle(SDL_Renderer *renderer, int x, int y, int radius) {
    int offsetX = 0;
    int offsetY = radius;
    int m = 5 - 4 * radius;

    while (offsetY >= offsetX) {
        SDL_RenderDrawPoint(renderer, offsetX + x, offsetY + y);
        SDL_RenderDrawPoint(renderer, offsetX + x, -offsetY + y);
        SDL_RenderDrawPoint(renderer, offsetY + x, offsetX + y);
        SDL_RenderDrawPoint(renderer, offsetY + x, -offsetX + y);
        SDL_RenderDrawPoint(renderer, -offsetX + x, offsetY + y);
        SDL_RenderDrawPoint(renderer, -offsetX + x, -offsetY + y);
        SDL_RenderDrawPoint(renderer, -offsetY + x, -offsetX + y);
        SDL_RenderDrawPoint(renderer, -offsetY + x, offsetX + y);

        if (m > 0) {
            offsetY--;
            m -= 8 * offsetY;
        }
        offsetX++;
        m += 8 * offsetX + 4;
    }
}

void fillCircle(SDL_Renderer *renderer, int x, int y, int radius) {
    int offsetX = 0;
    int offsetY = radius;
    int m = 5 - 4 * radius;

    while (offsetY >= offsetX) {
        SDL_RenderDrawLine(renderer, offsetX + x, -offsetY + y, offsetX + x, offsetY + y);
        SDL_RenderDrawLine(renderer, offsetY + x, offsetX + y, offsetY + x, -offsetX + y);
        SDL_RenderDrawLine(renderer, -offsetX + x, offsetY + y, -offsetX + x, -offsetY + y);
        SDL_RenderDrawLine(renderer, -offsetY + x, -offsetX + y, -offsetY + x, offsetX + y);

        if (m > 0) {
            offsetY--;
            m -= 8 * offsetY;
        }
        offsetX++;
        m += 8 * offsetX + 4;
    }
}

void drawCircle(SDL_Renderer *renderer, int x, int y, int radius) {
    drawOuterCircle(renderer, x, y, radius);
    fillCircle(renderer, x, y, radius);
}
