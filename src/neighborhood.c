#include "../include/neighborhood.h"

#include <stdio.h>

Neighbor *createNeighbor(int x, int y) {
    int i;
    Neighbor *neighbor = NULL;
    if ((neighbor = malloc(sizeof(Neighbor))) == NULL) {
        return NULL;
    }
    neighbor->x = x;
    neighbor->y = y;
    if ((neighbor->neighbors = malloc(sizeof(Neighbor*) * MAX_NEIGHBOR)) == NULL) {
        return NULL;
    }
    for (i = 0; i < MAX_NEIGHBOR; i++) {
        neighbor->neighbors[i] = NULL;
    }
    return neighbor;
}

int compareNeighbor(Neighbor *neighbor1, Neighbor *neighbor2) {
    int isXEqual = neighbor1->x == neighbor2->x ? 1 : 0;
    int isYEqual = neighbor1->y == neighbor2->y ? 1 : 0;
    return isXEqual && isYEqual;
}

void getNeighborhood(Board *board, Neighbor *neighbor, Neighbor *neighborhood) {
    int x = neighbor->x;
    int y = neighbor->y;
    int color = board->content[y][x];
    if ((x < board->nbCols -1) && (board->content[y][x + 1] == color) && !isNeighborAlreadyRegistered(neighborhood, x + 1, y)) {
        if ((neighbor->neighbors[RIGHT] = createNeighbor(x + 1, y)) != NULL) {
            getNeighborhood(board, neighbor->neighbors[RIGHT], neighborhood);
        }
    }
    if ((x > 0 && board->content[y][x - 1] == color) && !isNeighborAlreadyRegistered(neighborhood, x - 1, y)) {
        if ((neighbor->neighbors[LEFT] = createNeighbor(x - 1, y)) != NULL) {
            getNeighborhood(board, neighbor->neighbors[LEFT], neighborhood);
        }
    }
    if ((y > 0) && (board->content[y - 1][x] == color) && !isNeighborAlreadyRegistered(neighborhood, x, y - 1)) {
        if ((neighbor->neighbors[UP] = createNeighbor(x, y - 1)) != NULL) {
            getNeighborhood(board, neighbor->neighbors[UP], neighborhood);
        }
    }
    if ((y < board->nbRows - 1) && (board->content[y + 1][x] == color) && !isNeighborAlreadyRegistered(neighborhood, x, y + 1)) {
        if((neighbor->neighbors[DOWN] = createNeighbor(x, y + 1)) != NULL) {
            getNeighborhood(board, neighbor->neighbors[DOWN], neighborhood);
        }
    }
}

int isNeighborAlreadyRegistered(Neighbor *neighborhood, int x, int y) {
    int i;
    if (neighborhood == NULL) {
        return 0;
    }
    if (neighborhood->x == x && neighborhood->y == y) {
        return 1;
    }
    for (i = 0; i < MAX_NEIGHBOR; i++) {
        if (isNeighborAlreadyRegistered(neighborhood->neighbors[i], x, y)) {
            return 1;
        }
    }
    return 0;
}

void getNumberOfNeighbor(Neighbor *neighborhood, int *nbr) {
    int i;
    if (neighborhood == NULL) {
        return;
    }
    (*nbr)++;
    for (i = 0; i < MAX_NEIGHBOR; i++) {
        getNumberOfNeighbor(neighborhood->neighbors[i], nbr);
    }
}

void processNeighbors(GameData *gameData) {
    int x, y, nbrBalls;
    int neighborProcessed = 1;
    while (neighborProcessed) {
        neighborProcessed = 0;
        for (y = gameData->board->nbRows - 1; y >= 0; y--) {
            for (x = 0; x < gameData->board->nbCols; x++) {
                if (gameData->board->content[y][x] != 0) {
                    Neighbor *neighborhood = NULL;
                    neighborhood = createNeighbor(x, y);
                    getNeighborhood(gameData->board, neighborhood, neighborhood);
                    nbrBalls = 0;
                    getNumberOfNeighbor(neighborhood, &nbrBalls);
                    displayNeighborhood(neighborhood);
                    if (nbrBalls >= 4) {
                        int firstNeighborColor = gameData->board->content[neighborhood->y][neighborhood->x];
                        if (firstNeighborColor < gameData->availableColors->nbColors) {
                            firstNeighborColor++;
                        }
                        if (firstNeighborColor > gameData->availableColors->size - 1) {
                            enableColor(gameData->availableColors, firstNeighborColor);
                        }
                        removeNeighborhoodFromBoard(neighborhood, gameData->board);
                        setBoardCel(gameData->board, neighborhood->y, neighborhood->x, firstNeighborColor);
                        fillBoardGaps(gameData->board);
                        neighborProcessed++;
                    }
                    if (neighborhood != NULL) {
                        destroyNeighborhood(neighborhood);
                    }
                }
            }
        }
    }
}

void removeNeighborhoodFromBoard(Neighbor *neighbor, Board *board) {
    int i;
    board->content[neighbor->y][neighbor->x] = 0;
    for (i = 0; i < MAX_NEIGHBOR; i++) {
        if (neighbor->neighbors[i] != NULL) {
            removeNeighborhoodFromBoard(neighbor->neighbors[i], board);
        }
    }
}

void destroyNeighborhood(Neighbor *neighborhood) {
    int i;
    if (neighborhood->neighbors != NULL) {
        for (i = 0; i < MAX_NEIGHBOR; i++) {
            if (neighborhood->neighbors[i] != NULL) {
                destroyNeighborhood(neighborhood->neighbors[i]);
            }
        }
        free(neighborhood->neighbors);
    }
    free(neighborhood);
}

void displayNeighborhood(Neighbor *neighborhood) {
    int i;
    if (neighborhood == NULL) {
        return;
    }
    for (i = 0; i < MAX_NEIGHBOR; i++) {
        if (neighborhood->neighbors[i] != NULL) {
            displayNeighborhood(neighborhood->neighbors[i]);
        }
    }
}
