#include <stdlib.h>
#include "../include/board.h"

Board *createBoard(int nbRows, int nbCols) {
    int i, j;
    Board *board = NULL;

    if ((board = malloc(sizeof(Board))) == NULL) {
        return NULL;
    }
    board->nbRows = nbRows;
    board->nbCols = nbCols;
    if ((board->content = malloc(sizeof(int*) * nbRows)) == NULL) {
        return NULL;
    }
    for (i = 0; i < nbRows; i++) {
        if ((board->content[i] = malloc(sizeof(int) * nbCols)) == NULL) {
            return NULL;
        }
        for (j = 0; j < nbCols; j++) {
            board->content[i][j] = 0;
        }
    }
    return board;
}

int putMoleculeInBoard(Molecule *molecule, Board *board) {
    /* 1 - Déterminer si la molecule peut être placée */
    int ball1 = SDL_FALSE;
    int ball2 = SDL_FALSE;
    if (!board->content[0][molecule->pos]) {
        ball1 = SDL_TRUE;
    }
    if ((molecule->alignment == VERTICAL) && (!board->content[1][molecule->pos])) {
        ball2 = SDL_TRUE;
    }
    else if ((molecule->pos + 1 < board->nbCols) && (!board->content[0][molecule->pos + 1])) {
        ball2 = SDL_TRUE;
    }
    /* Si non => arrêter */
    if ((ball1 && ball2) == SDL_FALSE) {
        return 0;
    }
    /* Si oui la placer en haut du plateau*/
    setBoardCel(board, 0, molecule->pos, molecule->color);
    if (molecule->alignment == VERTICAL) {
        setBoardCel(board, 1, molecule->pos, molecule->color);    
    }
    else {
        setBoardCel(board, 0, molecule->pos + 1, molecule->color);
    }
    return 1;
}

void fillBoardGaps(Board *board) {
    int col;
    int change = -1;
    /* Le traitement s'arrête dès qu'aucune boule n'a été déplacée */
    while (change != 0) {
        change = 0;
        for (col = 0; col < board->nbCols; col++) {
            int row = board->nbRows - 1;
            while (board->content[row][col] && row > 0) {
                row--;
            }
            /* Si il y'a un trou dans la colonne traitée */
            if (row > 0) {
                int ballBeyondGap = getBallBeyondGap(board, row, col);
                /* Place la première boule au dessus du trou à la place du trou */
                if (ballBeyondGap != -1) {
                    setBoardCel(board, row, col, board->content[ballBeyondGap][col]);
                    setBoardCel(board, ballBeyondGap, col, 0);
                    change++;
                }
            }
        }
    }
}

int getBallBeyondGap(Board *board, int row, int col) {
    while (row >= 0 && board->content[row][col] == 0) {
        row--;
    }
    return row;
}

void setBoardCel(Board *board, unsigned int y, unsigned int x, unsigned int value) {
    if (x < board->nbCols && y < board->nbRows) {
        board->content[y][x] = value;
    }
}

void destroyBoard(Board *board) {
    int i;
    for (i = 0; i < board->nbRows; i++) {
        free(board->content[i]);
    }
    free(board->content);
    free(board);
}
