#include "../include/alchimiste.h"

int main(int argc, char *argv[]) {
    SDLRessources *sdlRessources;
    int exitStatus = EXIT_SUCCESS;    
    
    if (SDL_Init(SDL_INIT_VIDEO) == -1) {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError());
        return EXIT_FAILURE;
    }
    if ((sdlRessources = createSDLRessources()) != NULL) {
        gameLoop(sdlRessources);
    } else {
        exitStatus = EXIT_FAILURE;
    }
    cleanSDLRessources(sdlRessources);
    SDL_Quit();
    return exitStatus;
}
