NAME		= 	Alchimiste
FLAGS		=	-Wall -pedantic -I./include
GRAPH_FLAGS	= 	-lSDL2
OBJ_FOLDER	=	bin/
SRC_FOLDER	=	src/
OBJS		= 	main.o gameData.o circle.o alchimiste.o drawStage.o board.o \
				molecule.o neighborhood.o

$(NAME): all
	gcc -o $@ $(OBJS) $(FLAGS) $(GRAPH_FLAGS)
	mkdir -p $(OBJ_FOLDER)
	mv $(OBJS) ./$(OBJ_FOLDER)

all: 
	make main.o
	make gameData.o
	make alchimiste.o
	make circle.o
	make drawStage.o
	make board.o
	make molecule.o
	make neighborhood.o

main.o: $(SRC_FOLDER)main.c
	gcc -c $< $(FLAGS) -ansi

gameData.o: $(SRC_FOLDER)gameData.c
	gcc -c $< $(FLAGS) -ansi

alchimiste.o: $(SRC_FOLDER)alchimiste.c
	gcc -c $< $(FLAGS) -ansi

circle.o: $(SRC_FOLDER)circle.c
	gcc -c $< $(FLAGS) -ansi

drawStage.o: $(SRC_FOLDER)drawStage.c
	gcc -c $< $(FLAGS) -ansi

board.o: $(SRC_FOLDER)board.c
	gcc -c $< $(FLAGS) -ansi

molecule.o: $(SRC_FOLDER)molecule.c
	gcc -c $< $(FLAGS) -ansi

neighborhood.o: $(SRC_FOLDER)neighborhood.c
	gcc -c $< $(FLAGS) -ansi

re: fclean $(NAME)

clean:
	rm -rf $(OBJ_FOLDER)$(OBJS)

fclean: clean
	rm -f $(NAME)

.PHONY: re clean fclean
