#ifndef ALCHIMISTE_h_
#define ALCHIMISTE_h_

#include <SDL2/SDL.h>
#include <stdio.h>
#include "molecule.h"
#include "board.h"
#include "neighborhood.h"

#define W_TITLE     "Alchimiste"
#define W_WIDTH     1024
#define W_HEIGHT    720

#define BALL_SIZE   50
#define BOARD_ROWS  8
#define BOARD_COLS  6

#define BOARD_OFFSET 150

#define GREEN       1
#define ORANGE      2
#define RED         3
#define PURPLE      4
#define BLUE        5
#define LIGHT_GREEN 6
#define DARK_GREEN  7
#define YELLOW      8
#define GRAY        9
#define WHITE       10
#define BLACK       11

/**
 * SDLRessources est une structure qui contient les données 
 * dont la SDL à besoin pour travailler.
 * 
 * \param window La structure représentant la fenêtre de l'application.
 * \param renderer La strucutre qui permet de dessiner dans la fenêtre.
 * \param event La structure qui capture les évenements  (clavier, souris, etc.).
 */
typedef struct SDLRessources {
    SDL_Window      *window;
    SDL_Renderer    *renderer;
    SDL_Event       event;
} SDLRessources;

/**
 * Boucle de jeu principale
 * 
 * \param sdlRessources la structure sdlRessources contenant les ressources qui doivent
 *                      être exploitées par la SDL2.
 */
void gameLoop(SDLRessources *sdlRessources);

/**
 * Gère les evenèments d'entrée de l'utilisateur qui ont été capturés.
 * 
 * \param gameData Les données du jeu
 * \param event L'évenement qui doit être géré.
 */
void handleKeyEvent(GameData *gameData, SDL_KeyboardEvent event);

/**
 * Détermine si la partie en cours est perdue.
 * 
 * @param gameData Les données de la partie en cours
 * @return int 1 si la partie est perdu, sinon 0;
 */
int isGameOver(GameData *gameData);

/**
 * Initialise les variables d'une structure sdlRessources (SDL_Window et SDL_Renderer).
 * 
 * \returns Un pointeur sur la strcuture SDLRessources en cas de succès ou NULL en cas d'échec.
 */
SDLRessources  *createSDLRessources();

/**
 * Détruit proprement les ressources contenues dans une structure sdlRessources.
 * 
 * \param sdlRessources la structure dont les ressources doivent être détruites.
 */
void cleanSDLRessources(SDLRessources *sdlRessources);

/**
 * Pour un code couleur donnée, rempli une structure SDL_Color 
 * 
 * \param colorCode le code couleur dont on veut une structure SDL correspondante.
 * \param color La structure SDL_Color dans laquelle on stocke la couleur
 */ 
void getSDLColor(int colorCode, SDL_Color *color);
#endif
