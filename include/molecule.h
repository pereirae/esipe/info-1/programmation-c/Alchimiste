#ifndef MOLECULE_h_
#define MOLECULE_h_

#include <time.h>
#include <stdlib.h>

#define HORIZONTAL  0
#define VERTICAL    1

/**
 * Molecule est une structure représentant le couple de boules de même couleur
 * avec lequel le joueur intéragit.
 * \param pos La colone où se trouve la boule basse ou à gauche
 * \param alignment 0 = horizontale, 1 = verticale
 * \param color La couleur de la molecule
 */ 
typedef struct molecule {
    int pos;
    int alignment;
    int color;
} Molecule;

/**
 * Déplace vers la droite une molecule
 * 
 * \param molecule La molecule à déplacer
 */ 
void moveMoleculeRight(Molecule *molecule);

/**
 * Déplace vers la gauche une molecule
 * 
 * \param molecule La molecule à déplacer
 */
void moveMoleculeLeft(Molecule *molecule);

/**
 * Change la direction de la molecule (passe à l'horizontale si elle était à la verticale
 * et inversement).
 * 
 * \param molecule La molécule à tourner
 */
void rotateMolecule(Molecule *molecule);

/**
 * Créer une molecule à partir de sa couleur
 * 
 * \param color La couleur de la molécule
 * 
 * \returns Un pointeur sur la molécule crée
 */ 
Molecule *createMolecule(int color);

/**
 * Génère un entier aléatoire compris entre les bornes min et max
 * 
 * \param min La valeur minimale de l'entier à générer
 * \param max La valeur maximale de l'entier à générer
 * 
 * \returns L'entier aléatoire généré.
 */ 
int getRandomValueBetween(int min, int max);

/**
 * Libère la mémoire utilisée par une structure molecule
 * 
 * @param molecule La molecule à détruire
 */
void destroyMolecule(Molecule *molecule);

/**
 * AvailableColors représente les couleurs des molécules
 * qui peuvent apparaître à un moment T de la partie
 */
typedef struct availableColors {
    int size;
    int *colors;
    int nbColors;
} AvailableColors;

/**
 * Créer une structure availableColors
 * 
 * @param nbColors Le nombre de couleur maximum de couleur disponible
 * @return Un pointeur sur la structure crée
 */
AvailableColors *initAvailableColors(int nbColors);

/**
 * Définit le statut d'une couleur comme active
 * 
 * @param availableColors La liste des couleurs actives à modifier
 * @param color La couleur à activer
 */
void enableColor(AvailableColors *availableColors, int color);

/**
 * Détruit une structure AvailableColors en libérant les ressources
 * qu'elle utilise
 * 
 * @param availableColors Le pointeur vers la structure à détruire
 */
void destroyAvailableColors(AvailableColors *availableColors);

#endif
