#ifndef NEIGHBORHOOD_h_
#define NEIGHBORHOOD_h_

#include <stdlib.h>
#include "gameData.h"
#include "board.h"
#include "molecule.h"

#define UP      0
#define DOWN    1
#define LEFT    2
#define RIGHT   3

#define MAX_NEIGHBOR    4

/**
 * Neighbor est une structure qui représente un voisinage de plusieurs boules. C'est à dire une suite de boules qui sont adjacentes (diagonales exclues). Chaque boule peut avoir au maximum MAX_NEIGHBOR voisins.
 * 
 * \param x La position en x de la boule
 * \param y La position en y de la boule
 * \param neighbors Les voisins de la boule
 */
typedef struct neighbor {
    int x;
    int y;
    struct neighbor **neighbors;
} Neighbor;

/**
 * Créer un voisinage à partir des coordonées de la boule "racine" du voisinage.
 * 
 * \param x La position en x de la boule "racine"
 * \param y La position en y de la boule "racine"
 * \return Neighbor* Le pointeur sur la premier voisin du voisinage
 */
Neighbor *createNeighbor(int x, int y);

/**
 * Détruit (libère la mémoire) utilisée par un voisinage.
 * 
 * \param neighborhood Le voisinage à détruire
 */
void destroyNeighborhood(Neighbor *neighborhood);

/**
 * Compare deux voisins, si leurs coordonnées sont identiques, ils sont égaux.
 * 
 * \param neighbor1 La premier voisin
 * \param neighbor2 Le deuxième voisin
 * 
 * \return int 1 si les voisins sont égaux, sinon 0
 */
int compareNeighbor(Neighbor *neighbor1, Neighbor *neighbor2);

/**
 * Compte le nombre d'éléments d'un voisinage. La fonction est récursive, le compteur doit donc être déclaré en dehors de la fonction.
 * 
 * @param neighborhood Le voisinage dont on veut compter les éléments.
 * @param nbr L'adresse d'une variable servant de compteur.
 */
void getNumberOfNeighbor(Neighbor *neighborhood, int *nbr);

/**
 * Définie si un voisin est déjà présent dans un voisinage
 * 
 * @param neighborhood Le voisinage dans lequel chercher la présence d'un voisin
 * @param x La position en x du voisin
 * @param y La position en y du voisin
 * 
 * @return int 1 si le voisin est déjà présent dans le voisinage, sinon 0.
 */
int isNeighborAlreadyRegistered(Neighbor *neighborhood, int x, int y);

/**
 * Efface un voisinage du plateau de jeu
 * 
 * @param neighborhood Le voisinage à effacer du plateau de jeu
 * @param board Le plateau de jeu duquel effacer le voisinage
 */
void removeNeighborhoodFromBoard(Neighbor *neighborhood, Board *board);

/**
 * Traite (recherche et supprime si besoin) les voisinnages présent sur le plateau de jeu 
 * 
 * @param board Le plateau de jeu où chercher les voisinages
 */
void processNeighbors(GameData *gameData);

/**
 * Affiche un voisinage.
 * 
 * @param neighborhood Le voisinage à afficher
 */
void displayNeighborhood(Neighbor *neighborhood);

#endif
