#ifndef GAMEDATA_h_
#define GAMEDATA_h_

#include "board.h"
#include "molecule.h"

/**
 * Structure  qui représente la partie en cours.
 * 
 * \param board représente le plateau de jeu.
 * \param availableColors la liste des couleurs que peuvent avoir les molécules.
 * \param molecule le couple de boule (molecule) qui peut être placé sur le plateau de jeu.
 * 
 */
typedef struct gameData {
    Board *board;
    AvailableColors *availableColors;
    Molecule *molecule;
} GameData;

/**
 * Instancie une structure GameData, cette structure contient les
 * données relatives à la partie en cours.
 * 
 * \return GameData* Un pointeur sur la structure GameData
 */
GameData *initGameData();

/**
 * Libère les ressources utilisées par une structure GameData.
 * 
 * \param gameData La structure à détruire.
 */
void destroyGameData(GameData *gameData);

#endif
