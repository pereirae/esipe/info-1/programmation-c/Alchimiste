#ifndef CIRCLE_h_
#define CIRCLE_h_

#include <SDL2/SDL.h>

/**
 * Trace un cercle de rayon radius à avec le point {x,y} comme point central.
 * Le tracé est effectué avec l'algorithme de tracé de cercle de Bresenham.
 * 
 * \param renderer Le renderer SDL permettant de tracer le cercle.
 * \param x La position sur l'axe X du centre du cercle à tracer.
 * \param y La position sur l'axe Y du centre du cercle à tracer.
 * \param radius Le rayon du cercle à tracer.
 */
void drawOuterCircle(SDL_Renderer *renderer, int x, int y, int radius);

/**
 * Rempli un cercle de rayon radius dont le centre est aux coordonées {x,y}.
 * 
 * \param renderer Le renderer SDL permettant de tracer le cercle.
 * \param x La position sur l'axe X du centre du cercle à tracer.
 * \param y La position sur l'axe Y du centre du cercle à tracer.
 * \param radius Le rayon du cercle à tracer.
 */ 
void fillCircle(SDL_Renderer *renderer, int x, int y, int radius);

/**
 * Trace un cercle rempli de rayon radius dont le centre est {x,y}.
 * 
 * \param renderer Le renderer SDL permettant de tracer le cercle.
 * \param x La position sur l'axe X du centre du cercle à tracer.
 * \param y La position sur l'axe Y du centre du cercle à tracer.
 * \param radius Le rayon du cercle à tracer.
 */ 
void drawCircle(SDL_Renderer *renderer, int x, int y, int radius);

#endif
