#ifndef DRAWSTAGE_h_
#define DRAWSTAGE_h_

#include "./alchimiste.h"
#include "./circle.h"

/**
 * Dessine la totalité du jeu selon son état à un instant T
 * 
 * \param sdlRessources Les ressources SDL permettant de dessiner
 * \param gameData Les données de la partie en cours
 */ 
void drawStage(SDLRessources *sdlRessources, GameData *gameData);

/**
 * Dessine la liste des boules qui peuvent apparaître
 */ 
void drawAvailableBalls(SDLRessources *sdlRessources, AvailableColors *availableColors, int wHeight);

/**
 * Dessine le cadre du plateau de jeu.
 */ 
void drawBoardFrame(SDLRessources *sdlRessources);

/**
 * Déssine le contenu du plateu de jeu
 * 
 * \param sdlRessources Les ressources SDL permettant de gérer le dessin
 * \param board Une structure Board représentant le tableau de jeux
 * \param windowHeight La hauteur de la fênetre
 */ 
void drawBoardContent(SDLRessources *sdlRessources, Board *board, int windowHeight);

/**
 * Désinne 
 * 
 * @param sdlRessources 
 * @param molecule 
 */
void drawMolecule(SDLRessources *sdlRessources, Molecule *molecule);

/**
 * Sauvegarde la couleur actuelle du renderer
 * 
 * \param renderer le renderer dont la couleur doit être sauvegardée
 * \param color la structure SDL_Color dans laquelle la coueleur est sauvegardée
 */ 
void getCurrentRenderColor(SDL_Renderer *renderer, SDL_Color *color);

/**
 * Change la couleur de dessin du renderer
 * 
 * \param renderer le renderer dont la couleur est à changer
 * \param color la nouvelle couleur de dessin du renderer
 */ 
void setCurrentRenderColor(SDL_Renderer *renderer, SDL_Color *color);

#endif
