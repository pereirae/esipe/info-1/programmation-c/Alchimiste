#ifndef BOARD_h_
#define BOARD_h_

#include <SDL2/SDL.h>
#include "molecule.h"

/**
 * Board est une structure qui réprésente un plateau de jeu
 * 
 * \param content Un tableau 2D d'int représentant le contenu du plateau de jeu
 * \param nbRows Le nombre de ligne du plateau de jeu
 * \param nbCols Le nombre de colones du plateau de jeu
 */
typedef struct board {
    int **content;
    int nbRows;
    int nbCols;
} Board;

/**
 * Instancie une structure Board et initialise ses membres.
 * 
 * \param nbRows Le nombre de lignes du plateau de jeu
 * \param nbCols Le nombre de colonne du plateau de jeu
 * \returns Un pointeur vers la structure Board créer
 */
Board *createBoard(int nbRows, int nbCols);

/**
 * Place une molecule dans un plateau de jeu, si il n'y a pas de place, ne fait rien
 * \param molecule La molecule à placer dans un plateau de jeu
 * \param board Le plateau de jeu dans lequel placé la molecule
 * 
 * \return 1 si la molecule à pu être placé, sinon 0
 */
int putMoleculeInBoard(Molecule *molecule, Board *board);

/**
 * Comble les vides entre plusieurs boules d'un plateau de jeu. C'est la fonction
 * qui gère l'effet "gravité" du jeu.
 * 
 * \param board Le plateau de jeu à traiter
 */
void fillBoardGaps(Board *board);

/**
 * Recherche la première ligne (depuis un index donné) où se situe une boule
 * et renvoie son index
 * 
 * \param board Le plateau où effectué la recherche
 * \param row La ligne de départ de la recherche
 * \param col La colone où est effectué la recherche
 * 
 * \return L'index de la première boule trouvé, sinon -1.
 */
int getBallBeyondGap(Board *board, int row, int col);

/**
 * Positionne une boule aux coordonnées x, y de la couleur value sur le plateau board.
 * 
 * \param board Le plateau de jeu à modifier.
 * \param y La coordonnée y de la position de la boule
 * \param x La coordonnée x de la position de la boule.
 * \param value La couleur de la boule se situant à la position transmise (0 = pas de boule)
 */ 
void setBoardCel(Board *board, unsigned int x, unsigned int y, unsigned int value);

/**
 * Détruit une structure Board en libérant les ressources qu'elle utilise.
 * 
 * \param board Le pointeur vers la structure Board à détruire.
 */ 
void destroyBoard(Board *board);

#endif
